package calculadora;

import static java.lang.Math.*;

public class Operaciontrigonometrica 
{
    double numero;
   
            
   double seno()
   {
       double numeroradi=Math.toRadians(numero);
       return sin(numeroradi);
   }
   
   double coseno()
   {
       double numeroradi=Math.toRadians(numero);
       return cos(numeroradi);
   }
   
   double tangente()
   {
       double numeroradi=Math.toRadians(numero);
       return tan (numeroradi);
   }
}
